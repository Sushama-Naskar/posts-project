
import React,{Component} from 'react';
import Userpost from './components/Userpost';


class App extends Component {
  constructor(props){
    super(props);
    this.state={
      users:null,
      posts:null,
      comments:null,
      status:false
     
      
    }
  }
  componentDidMount(){
    fetch("https://jsonplaceholder.typicode.com/users").then((res)=>{
      return res.json();
    }).then((data)=>{
      
      this.setState({
        users:data,
        
      })
      return fetch("https://jsonplaceholder.typicode.com/posts");
    }).then((res)=>{
      return res.json();
    }).then((data)=>{
      
      this.setState({
        posts:data,
        
      })
      return fetch("https://jsonplaceholder.typicode.com/comments");
    }).then((res)=>{
      return res.json();
    }).then((data)=>{
      
      this.setState({
        comments:data,
        status:true
       
      })
      
    })

   
  }
  getUserName=(userid)=>{
        const {users,comments,posts,status}={...this.state};
        
        if(status){
          let name=users.find((user)=>{
          
            return user.id==userid;
          })
         
          return name.name;
        }
    }

  render(){
   
   
   
    return (
      <div className="App">
     { (this.state.status)? <div>
      <Userpost users={this.state.users} posts={this.state.posts} comments={this.state.comments} getUserName={this.getUserName}/>
       </div>:null}
          
      </div>
    );

    }
 
}

export default App;

