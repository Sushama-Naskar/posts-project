import React, { Component } from 'react';
import Comment from './Comment';
import User from './Users';
import './userpost.css';


export default class Post extends Component {
    constructor(props){
        super(props);
        this.state={
            list:null,
            received:false
        }
       
    }


    render() {
        
        return (
            <div>
            {
                this.props.posts.map((element)=>{
                    return(
                        <div key={element.id} className="main-container">
                        <div className='user-container'>
                       <User userId={element.userId} Users={this.props.users} />
                       </div>
                       <div className="post-container"> 
                        <div><h2>{element.title}</h2></div>
                        <div>{element.body}</div>
                        </div>
                        <div className="comment-container">
                        <Comment postId={element.id} comments={this.props.comments}/>
                        </div>
                        </div>
                    
                    )
                })
            }
           
           </div>
        )
    }
}

