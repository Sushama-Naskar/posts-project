import React, { Component } from 'react';
import Usericon from '../images/user.png';
import './user.css'

export default class Users extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }
    getUserName=()=>{

    }

    render() {
        return (
            <div>
            {this.props.Users.map((Element)=>{
                if(Element.id===this.props.userId){
                    return(
                        <div key={Element.id}>
                        <div className='user-details'>
                        <div className='user-icon'><img src={Usericon} className='user-icon'/></div>
                        <div className='user-name'>
                        <div className="padding-bottom-6">@{Element.username}</div>
                           <div>{Element.name}</div>
                           </div>
                           </div>
                           
                        </div>
                    )
                }
                
            })}
                
            </div>
        )
    }
}
